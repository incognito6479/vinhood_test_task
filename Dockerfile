FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY Pipfile /code/
COPY Pipfile.lock /code/
RUN pip install pipenv
# RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --dev
RUN pipenv install --system --deploy --ignore-pipfile
# RUN pipenv install --dev --ignore-pipfile
COPY . /code/
EXPOSE 8000