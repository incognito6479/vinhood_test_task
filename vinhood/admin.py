from django.contrib import admin
from vinhood.models import User, Category, Currency, WishList, Product


@admin.register(User)
class UserModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Currency)
class CurrencyModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    pass


@admin.register(WishList)
class WishListModelAdmin(admin.ModelAdmin):
    pass




