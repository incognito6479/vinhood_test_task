from django.apps import AppConfig


class VinhoodConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vinhood'
