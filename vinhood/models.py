from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Users require an email field')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = models.CharField(unique=True, max_length=256, blank=True, null=True)
    email = models.EmailField(unique=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return f"{self.email}"


class Category(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return f"{self.name}"


class Currency(models.Model):
    name = models.CharField(max_length=256)
    symbol = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.name} / {self.symbol}"


class Product(models.Model):
    name = models.CharField(max_length=256)
    price = models.FloatField()
    currency = models.ForeignKey('Currency', on_delete=models.PROTECT)
    category = models.ForeignKey('Category', on_delete=models.PROTECT)
    rank = models.FloatField()
    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name} {self.price} {self.currency}"

    def save(self, *args, **kwargs):
        self.price = round(self.price, 2)
        self.rank = round(self.rank, 1)
        return super(Product, self).save()


class WishList(models.Model):
    product = models.ForeignKey('Product', on_delete=models.PROTECT)
    user = models.ForeignKey('vinhood.User', on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.product} {self.user}"

