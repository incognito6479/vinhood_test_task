from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate
from vinhood.models import User, Product, Category, Currency, WishList


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all())
    currency = serializers.PrimaryKeyRelatedField(queryset=Currency.objects.all())

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'rank', 'category', 'currency']


class ProductListSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False)

    class Meta:
        model = Product
        fields = ['name', 'price', 'category', 'created_time']


class WishListCreateSerializer(serializers.Serializer):
    product_ids = serializers.ListField(child=serializers.IntegerField())


class UserAuthSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def create(self, validated_data):
        user = authenticate(email=validated_data['email'], password=validated_data['password'])
        if user is not None:
            token = RefreshToken.for_user(user)
            return {'refresh': str(token), 'access': str(token.access_token), 'user_id': user.id}
        return {'error': 'email or password is incorrect'}


class UserSignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'password']

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super().create(validated_data)


class UserPasswordResetSerializer(serializers.Serializer):
    password = serializers.CharField()

    def create(self, validated_data):
        instance = User.objects.filter(id=self.context['request'].user.id)\
            .update(password=make_password(validated_data['password']))
        return instance


class WishListSerializer(serializers.ModelSerializer):
    user = UserSignUpSerializer(many=False, read_only=True)
    product = ProductSerializer(many=False)

    class Meta:
        model = WishList
        fields = "__all__"
