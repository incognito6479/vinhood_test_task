from rest_framework import routers
from django.urls import path
from vinhood.views import WishListViewSet, UserAuthApiView, UserSignUpApiView, UserPasswordResetApiView, ProductViewSet, \
    CategoryViewSet, CurrencyViewSet

router = routers.SimpleRouter()
router.register('wishlist', WishListViewSet)
router.register('product', ProductViewSet)
router.register('category', CategoryViewSet)
router.register('currency', CurrencyViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('user/auth', UserAuthApiView.as_view(), name="user_auth"),
    path('user/sign-up', UserSignUpApiView.as_view(), name="user_sign_up"),
    path('user/reset-password', UserPasswordResetApiView.as_view(), name="user_reset_password"),
]
