from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status, permissions, viewsets

from vinhood.filters import ProductModelFilter
from vinhood.serializers import UserAuthSerializer, UserSignUpSerializer, UserPasswordResetSerializer, \
    WishListSerializer, ProductSerializer, CategorySerializer, CurrencySerializer, ProductListSerializer, \
    WishListCreateSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from vinhood.models import WishList, Product, Category, Currency


class UserAuthApiView(GenericAPIView):
    serializer_class = UserAuthSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request) -> Response:
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            response = serializer.save()
            status_ = status.HTTP_200_OK
            if 'error' in response:
                status_ = status.HTTP_404_NOT_FOUND
            return Response(response, status=status_)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserSignUpApiView(GenericAPIView):
    serializer_class = UserSignUpSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request) -> Response:
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            token = RefreshToken.for_user(instance)
            context = {'refresh': str(token),
                       'access': str(token.access_token),
                       'user_id': instance.id}
            return Response(context, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserPasswordResetApiView(GenericAPIView):
    serializer_class = UserPasswordResetSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request) -> Response:
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WishListViewSet(viewsets.ModelViewSet):
    """Wish List ViewSet"""
    queryset = WishList.objects.all()
    serializer_class = WishListCreateSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    http_method_names = ['get', 'post', 'delete']
    filterset_fields = ['user_id']

    def list(self, request, *args, **kwargs) -> Response:
        """Return wish listed products based on user authentication"""
        if request.user.is_authenticated:
            queryset = WishList.objects.filter(user=request.user)
        else:
            queryset = WishList.objects.filter(user_id=request.GET.get('user_id'))
        self.queryset = queryset
        self.serializer_class = WishListSerializer
        return super(WishListViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs) -> Response:
        """Create wish listed products, check if product category is not duplicated"""
        serializer = WishListCreateSerializer(data=request.data)
        if serializer.is_valid():
            # get all user wish listed products category ids
            category_id_in_db: object = WishList.objects.filter(user_id=request.user.id) \
                .values('product__category_id').distinct()
            # get all products needed to add
            products_in_db: object = Product.objects.filter(id__in=request.data['product_ids'])
            # variable to store product id connected to category id
            product_id_category_id: dict[int: int] = {}
            # variable to store added category ids by user
            category_id_in_db_list: list = []
            # add product ids and category ids
            for product in products_in_db:
                product_id_category_id[product.id] = product.category_id
            # add category ids added by user
            for category in list(category_id_in_db):
                category_id_in_db_list.append(category['product__category_id'])
            # check if products' category ids needed to add is not present in database
            if any(category_id in category_id_in_db_list for category_id in product_id_category_id.values()) or \
                    len(product_id_category_id) == 0:
                return Response({'error': 'product category already exists'},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                # create multiple wish listed products at one request
                wish_list_bulk_create: list = []
                for product_id in request.data['product_ids']:
                    wish_list_bulk_create.append(
                        WishList(
                            product_id=product_id,
                            user_id=request.user.id
                        )
                    )
                WishList.objects.bulk_create(wish_list_bulk_create)
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filterset_class = ProductModelFilter

    def list(self, request, *args, **kwargs) -> Response:
        serializer = ProductListSerializer(
            self.filter_queryset(self.queryset),
            many=True
        )
        return Response(serializer.data)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'post', 'delete']


class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'post', 'delete']
