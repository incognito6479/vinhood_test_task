from django_filters import rest_framework as filters
from vinhood.models import Product


class ProductModelFilter(filters.FilterSet):
    ORDER_BY_CHOICES = (
        ('asc', 'asc'),
        ('desc', 'desc')
    )

    created_time = filters.ChoiceFilter(choices=ORDER_BY_CHOICES, method='order_by_created_time')
    rank = filters.ChoiceFilter(choices=ORDER_BY_CHOICES, method='order_by_rank')

    class Meta:
        model = Product
        fields = {
            'price': ['lt', 'gt'],
        }

    def order_by_rank(self, queryset, *args, **kwargs):
        if args:
            queryset = queryset.order_by('rank')
            if args[1] == "desc":
                queryset = queryset.order_by('-rank')
        return queryset

    def order_by_created_time(self, queryset, *args, **kwargs):
        if args:
            queryset = queryset.order_by('created_time')
            if args[1] == "desc":
                queryset = queryset.order_by('-created_time')
        return queryset


