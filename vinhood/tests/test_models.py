from django.test import TestCase
from vinhood.models import User, Currency, Product, Category, WishList


class CurrencyModelTestCase(TestCase):
    def setUp(self) -> None:
        Currency.objects.create(name="USD", symbol="$")

    def test_currency_creation(self):
        currency = Currency.objects.get(name="USD")
        self.assertEqual(currency.name, 'USD')


class CategoryModelTestCase(TestCase):
    def setUp(self) -> None:
        Category.objects.create(name="Fruits")

    def test_currency_creation(self):
        category = Category.objects.get(name="Fruits")
        self.assertEqual(category.name, 'Fruits')


class ProductModelTestCase(TestCase):
    def setUp(self) -> None:
        product = Product.objects.create(name="Banana",
                                         price=14.50,
                                         currency=Currency.objects.create(name="USD", symbol="$"),
                                         category=Category.objects.create(name="Fruits"),
                                         rank=3.5)

    def test_currency_creation(self):
        product = Product.objects.get(name="Banana")
        self.assertEqual(product.name, 'Banana')


class UserModelTestCase(TestCase):
    def setUp(self) -> None:
        User.objects.create_user(email="example@example.com", password="123")

    def test_user_creation(self):
        user = User.objects.get(email="example@example.com")
        self.assertEqual(user.email, 'example@example.com')


class WishListModelTestCase(TestCase):
    def setUp(self) -> None:
        product = Product.objects.create(name="Banana",
                                         price=14.50,
                                         currency=Currency.objects.create(name="USD", symbol="$"),
                                         category=Category.objects.create(name="Fruits"),
                                         rank=3.5)
        user = User.objects.create_user(email="example@example.com", password="123")
        WishList.objects.create(product=product, user=user)

    def test_currency_creation(self):
        wish_list = WishList.objects.get(id=1)
        self.assertEqual(wish_list.product.name, 'Banana')
