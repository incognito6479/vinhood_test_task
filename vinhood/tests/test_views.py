from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from vinhood.models import User, Currency, Category, Product, WishList

USER_LOGIN_URL = reverse('user_auth')
USER_SIGN_UP_URL = reverse('user_sign_up')
USER_PASS_RESET_URL = reverse('user_reset_password')


class UserApiViewTestCase(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.user = User.objects.create_user(email="test@test.com", password="123")
        self.client.force_authenticate(self.user)

    def test_user_login_check(self):
        payload_1 = {'email': "test@test.com", 'password': "123"}
        payload_2 = {'email': "user@user_123.com", 'password': "123"}
        response = self.client.post(USER_LOGIN_URL, payload_1)
        response_error = self.client.post(USER_LOGIN_URL, payload_2)
        self.assertEqual(response_error.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(response.status_code, [status.HTTP_200_OK, status.HTTP_404_NOT_FOUND])

    def test_user_sign_up(self):
        payload = {'email': "test12@test12.com", 'password': "123"}
        response_success = self.client.post(USER_SIGN_UP_URL, payload)
        response_exists = self.client.post(USER_SIGN_UP_URL, payload)
        self.assertEqual(response_success.status_code, status.HTTP_200_OK)
        self.assertEqual(response_exists.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_password_reset(self):
        payload = {'password': "123456"}
        response = self.client.post(USER_PASS_RESET_URL, payload)
        response_fail = self.client.post(USER_PASS_RESET_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_fail.status_code, status.HTTP_400_BAD_REQUEST)


class ViewSetsTestCase(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.user = User.objects.create_user(email="test@test.com", password="123")
        self.client.force_authenticate(self.user)

    def test_category_viewset(self):
        self.client.post('/category/', {"name": "Fruits"})
        obj = Category.objects.filter(name="Fruits")
        self.assertTrue(obj.exists())
        response = self.client.delete(f'/category/{obj.first().id}/', format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_currency_viewset(self):
        self.client.post('/currency/', {"name": "USD", "symbol": "$"})
        obj = Currency.objects.filter(name="USD")
        self.assertTrue(obj.exists())
        response = self.client.delete(f'/currency/{obj.first().id}/', format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_product_viewset(self):
        cat_obj = Category.objects.create(name="Test_category")
        cur_obj = Currency.objects.create(name="USD", symbol="$")
        payload = {"name": "Test_product",
                   "price": 3.45,
                   "rank": 5,
                   "currency": cur_obj.id,
                   "category": cat_obj.id
                   }
        response_create = self.client.post("/product/", payload, format="json")
        obj = Product.objects.filter(name="Test_product")
        response_delete = self.client.delete(f"/product/{obj.first().id}/", format="json")
        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)

    def test_wish_list_viewset(self):
        cat_obj_1 = Category.objects.create(name="Test_category")
        cat_obj_2 = Category.objects.create(name="Test_category")
        product_obj_1 = Product.objects.create(
                    name="Test 1 product",
                    category=cat_obj_1,
                    price=3.45,
                    rank=5,
                    currency=Currency.objects.create(name="USD", symbol="$")
                )
        product_obj_2 = Product.objects.create(
            name="Test 2 product",
            category=cat_obj_2,
            price=3.45,
            rank=5,
            currency=Currency.objects.create(name="USD", symbol="$")
        )
        product_obj_3 = Product.objects.create(
            name="Test 3 product",
            category=cat_obj_1,
            price=3.45,
            rank=5,
            currency=Currency.objects.create(name="USD", symbol="$")
        )
        payload = {
            'product_ids': [product_obj_1.id, product_obj_2.id]
        }
        response_success = self.client.post('/wishlist/', payload, format="json")
        self.assertEqual(response_success.status_code, status.HTTP_201_CREATED)
        response_fail = self.client.post('/wishlist/', {"product_ids": [product_obj_3.id]}, format="json")
        self.assertEqual(response_fail.status_code, status.HTTP_406_NOT_ACCEPTABLE)
        wishlist_obj = WishList.objects.filter(product_id=product_obj_1, user=self.user)
        response_delete = self.client.delete(f"/wishlist/{wishlist_obj.first().id}/", format="json")
        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)
